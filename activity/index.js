function oddEvenChecker (number){
	if (typeof number === "number") {

		if (number % 2) {
			console.log("The number is odd")
		} else{
			console.log("The number is even")
		}

	} else {
		alert("Invalid Input")
	};
};

oddEvenChecker(16);



function budgetChecker(number) {
	if (typeof number === "number") {
		if (number >= 40000) {
			console.log("You are over the budget")
		} else if (number <= 40000) {
			console.log("You have resources left.")
		}
	} else{
		alert("Invalid Input")
	};
};

budgetChecker(50000000);
